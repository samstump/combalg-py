.. combalg-py documentation master file, created by
    sphinx-quickstart on Tue Jan 17 20:43:40 2017.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

==========
combalg-py
==========
**combalg-py** is a collection of combinatorial algorithms in python.  The
algorithms are primarily involve enumeration and *random selection*
of combinatorial objects like sets, subsets, permutations, partitions and
compositions.  Random selection is always (unless noted otherwise)
*uniformly at random*.  The majority of the initial implementation was
adapted from the mathematics presented in [NW1978]_.

.. toctree::
    :maxdepth: 4
    :caption: API Documentation:

.. autosummary::

    combalg.subset
    combalg.combalg
    combalg.rooted_tree
    combalg.utility



.. automodule:: combalg.subset
    :members:
    :show-inheritance:

.. automodule:: combalg.combalg
    :show-inheritance:

.. autoclass:: combalg.combalg.permutation
    :members:

.. autoclass:: combalg.combalg.composition
    :members:

.. autoclass:: combalg.combalg.integer_partition
    :members:

.. autoclass:: combalg.combalg.set_partition
    :members:

.. automodule:: combalg.rooted_tree
    :members:
    :show-inheritance:

.. automodule:: combalg.utility
    :members:
    :show-inheritance:

Todo
----
.. todolist::

References
----------
.. [NW1978] "Combinatorial Algorithms (for Computers and Calculators),2nd ed.", Nijenhuis & Wilf, Academic Press, 1978.
   http://www.math.upenn.edu/~wilf/website/CombinatorialAlgorithms.pdf

.. [ZS1998] "Fast Algorithms for Generating Integer Partitions", Zoghbi & Stojmenovic, 1998.
   https://pdfs.semanticscholar.org/9613/c1666b5e48a5035141c8927ade99a9de450e.pdf

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

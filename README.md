combalg-py
==========
Combinatorial Algorithms in Python
----------------------------------

**combalg-py** is a collection of combinatorial algorithms in python.
The algorithms are primarily involve enumeration and *random selection*
of combinatorial objects like sets, subsets, permutations, partitions
and compositions.  Random selection is always (unless noted otherwise)
*uniformly at random*.  The majority of the initial implementation was
adapted from the mathematics presented in [NW1978].

I was exposed to these algorithms very early in my career.  The code
seems very old to me, and the algorithms mysterious.  Here are some
examples.  The ways to sum 5 with positive integers:

    list(combalg.integer_partition.all(5))
    [[5], [4, 1], [3, 2], [3, 1, 1], [2, 2, 1], [2, 1, 1, 1], [1, 1, 1, 1, 1]]

A random permutation of 'january'

    ''.join(combalg.permutation.random('january'))
    'rauynja'

The number of ways to partition a set with 3 elements:

    {{a,b,c}}, {{a,b},{c}}, {{a,c},{b}}, {{a},{b,c}}, {{a},{b},{c}}

There is much more to say.  The most important are:

**combalg-py** uses:

* [pygradle](https://github.com/linkedin/pygradle): a python plugin for
  gradle build automation.
* [sphinx autodoc](http://www.sphinx-doc.org/en/1.5.1/ext/autodoc.html)
  for [documentation](http://pythonhosted.org/combalg-py/), which turned out pretty sweet.



References
----------
[NW1978] "Combinatorial Algorithms (for Computers and Calculators),2nd ed.", Nijenhuis & Wilf, Academic Press, 1978.
   http://www.math.upenn.edu/~wilf/website/CombinatorialAlgorithms.pdf


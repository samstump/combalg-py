import sys, os

# Make sure that the application source directory (this directory's parent) is
# on sys.path.

sys.path.insert(0, os.path.abspath('../src/'))
sys.path.insert(0, os.path.abspath('src/'))

import combalg.utility as utility
import combalg.subset as subset
import combalg.rooted_tree as rooted_tree
import combalg.combalg as combalg



import time
import math
import unittest
from functools import reduce


def map_set_partition(a, cls):
  y = []
  for t in range(len(a)):
    x = reduce(lambda x,y: x+[a[y]] if cls[y] == t else x, range(len(a)), [])
    if x:
      y.append(x)
  return y

  
# TODO: the generator tests should test for uniqueness

class TestCombalgFunctions(unittest.TestCase):
  def test_powerset(self):
    n = 15
    i = 0
    a = range(n)
    for t in subset.powerset(a):
      self.assertTrue(set(t) <= set(a))
      i += 1
    self.assertTrue(i == 2**n)

  def test_random_subset(self):
    n = 15
    a = range(n)
    trials = 100
    for i in range(trials):
      t = subset.random(a)
      self.assertTrue(set(t) <= set(a))
      
  def test_all_k_subsets(self):
    n = 15
    k = 6
    a = range(n)
    i = 0
    for t in subset.all_k_element(a, k):
        self.assertTrue(len(t) == k)
        self.assertTrue(set(t) <= set(a))
        i += 1
    self.assertTrue(i == combalg.binomial_coefficient(n,k))
    
  def test_random_k_subset(self):
    n = 15
    k = 6
    a = range(n)
    trials = 100
    for i in range(trials):
      t = subset.random_k_element(a, k)
      self.assertTrue(len(t) == k)
      self.assertTrue(set(t) <= set(a))
      
  def test_compositions(self):
    n = 15
    k = 6
    for t in combalg.composition.all(n,k):
      self.assertTrue(len(t) == k)
      self.assertTrue(sum(t) == n)
  
  def test_random_compositions(self):
    n = 15
    k = 6
    trials = 100
    for i in range(trials):
      t = combalg.composition.random(n, k)
      self.assertTrue(len(t) == k)
      self.assertTrue(sum(t) == n)
      
  def test_permutations(self):
    n = 8
    a = list(range(n))
    for t in combalg.permutation.all(a):
      self.assertTrue(len(t) == n)
      self.assertTrue(sorted(t) == a)

  def test_random_permutation(self):
    n = 6
    trials = 5 * math.factorial(n)
    a = list(range(n))
    for i in range(trials):
      t = combalg.permutation.random(a)
      self.assertTrue(len(t) == n)
      self.assertTrue(sorted(t) == a)

  def test_integer_partitions(self):
    n = 15
    for t in combalg.integer_partition.all(n):
      self.assertTrue(sum(t) == n)
  
  def test_random_integer_partition(self):
    n = 15
    trials = 100
    for i in range(trials):
      t = combalg.integer_partition.random(n)
      self.assertTrue(sum(t) == n)
      
  def test_set_partitions(self):
    a = list(range(6))
    n = len(a)
    for pop,cls,nc in combalg.set_partition.all(n):
      y = map_set_partition(a, cls)
      # union of all partition elements == a
      self.assertTrue(set().union(*y) == set(a))
      # intersection of all partitions == {}
      self.assertTrue(set().intersection(*y) == set())

  def test_random_set_partitions(self):
    a = ['a','b','c','d','e','f']
    trials = 100
    for i in range(trials):
      # let p = the given set partition
      p = combalg.set_partition.random(a)
      # union of members of p == a
      self.assertTrue(set().union(*p) == set(a))
      # intersection of members of p == {}
      self.assertTrue(set().intersection(*p) == set())
      
  def test_random_rooted_tree(self):
    n = 10
    trials = 1000
    for i in range(trials):
      t = rooted_tree.random(n)
      edge_list = []
      for j in range(1,len(t)):
        edge_list.append((j,t[j]))
      self.assertTrue(len(edge_list) == n-1)
      self.assertTrue(utility.is_tree(n,edge_list))
#
#
#
# execute
#suite = unittest.TestLoader().loadTestsFromTestCase(TestCombalgFunctions)
#unittest.TextTestRunner(verbosity=2).run(suite)

